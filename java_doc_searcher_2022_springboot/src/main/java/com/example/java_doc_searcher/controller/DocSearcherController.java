package com.example.java_doc_searcher.controller;

import com.example.java_doc_searcher.searcher.DocSearcher;
import com.example.java_doc_searcher.searcher.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
//@RequestMapping("/")
public class DocSearcherController {
//    @Autowired
    private static DocSearcher searcher=new DocSearcher();
//    @Autowired
    private ObjectMapper objectMapper=new ObjectMapper();
    @RequestMapping(value = "/searcher",produces = "application/json;charset=utf-8")
    @ResponseBody
    public String search(@RequestParam("query") String query, HttpServletResponse resp) throws JsonProcessingException {
        List<Result> results=searcher.search(query);
       String str=objectMapper.writeValueAsString(results);
       return str;
    }
}
