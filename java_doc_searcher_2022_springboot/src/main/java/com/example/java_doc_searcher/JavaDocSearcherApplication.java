package com.example.java_doc_searcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaDocSearcherApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaDocSearcherApplication.class, args);
    }

}
