package com.example.java_doc_searcher.searcher;

public class Weight {
    private int docId;
    private int weight;

    @Override
    public String toString() {
        return "Weight{" +
                "docId=" + docId +
                ", weight=" + weight +
                '}';
    }

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
