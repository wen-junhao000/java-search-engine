package com.example.java_doc_searcher.searcher;

import lombok.Data;

@Data
public class Result {
    private String title;
    private String url;
    private String desc;
}
