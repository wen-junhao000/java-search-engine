import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.util.List;
import java.util.Scanner;

public class TestAnsj {
    public static void main(String[] args) {
        String str="小马毕业了66das6d5as654";
        List<Term> terms=ToAnalysis.parse(str).getTerms();
        for (Term term:terms) {
            System.out.println(term.getName());
        }
    }
}
