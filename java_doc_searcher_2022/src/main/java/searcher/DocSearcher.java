package searcher;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class DocSearcher {
    private static final String STOP_WORD_PATH="/root/java2/javaproject/stop_word.txt";
    private HashSet<String> stopWords =new HashSet<>();
    Index index=new Index();
    public DocSearcher(){
        index.load();
        loadStopWords();
    }
    public List<Result> search(String query){
        List<Term> oldTerms= ToAnalysis.parse(query).getTerms();
        List<Term> terms= new ArrayList<>();
        for (Term term:oldTerms) {
            if (!stopWords.contains(term.getName())){
                terms.add(term);
            }else {
                continue;
            }
        }
//        List<Weight> allTermResult=new ArrayList<>();
        List<Result> results=new ArrayList<>();
        List<List<Weight>> termResult=new ArrayList<>();
        for (Term term:terms) {
            String word=term.getName();
            List<Weight> invertedList=index.getInverted(word);
            if (invertedList==null){
                continue;
            }
            termResult.add(invertedList);
        }

        List<Weight> allTermResult=mergeResult(termResult);
        allTermResult.sort(new Comparator<Weight>() {
            @Override
            public int compare(Weight o1, Weight o2) {
                return o2.getWeight()-o1.getWeight();
            }
        });
        for (Weight weight:allTermResult) {
            DocInfo docInfo=index.getDocInfo(weight.getDocId());
            Result result=new Result();
            result.setTitle(docInfo.getTitle());
            result.setUrl(docInfo.getUrl());
            result.setDesc(GetDesc(docInfo.getContent(),terms));
            results.add(result);
        }
        return results;
    }
    static class Pos{
        public int row;
        public int col;
        public Pos(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }
    private List<Weight> mergeResult(List<List<Weight>> source) {
        for (List<Weight> curRow:source) {
            curRow.sort((o1, o2) -> o1.getDocId()-o2.getDocId());
        }
        List<Weight> target=new ArrayList<>();
        PriorityQueue<Pos> queue=new PriorityQueue<>(new Comparator<Pos>() {
            @Override
            public int compare(Pos o1, Pos o2) {
                Weight w1=source.get(o1.row).get(o1.col);
                Weight w2=source.get(o2.row).get(o2.col);
                return w1.getDocId()-w2.getDocId();
            }
        });

        for (int row = 0; row < source.size(); row++) {
            queue.offer(new Pos(row,0));
        }
        while (!queue.isEmpty()){
            Pos minPos=queue.poll();
            Weight curWeight=source.get(minPos.row).get(minPos.col);
            if (target.size()>0){
                Weight lastWeight=target.get(target.size()-1);
                if (lastWeight.getDocId()==curWeight.getDocId()){
                    lastWeight.setWeight(lastWeight.getWeight()+curWeight.getWeight());
                    target.remove(target.size()-1);
                    target.add(lastWeight);
                }else {
                    target.add(curWeight);
                }
            }else {
                target.add(curWeight);
            }
            Pos newPos=new Pos(minPos.row,minPos.col+1);
            if (newPos.col>=source.get(newPos.row).size()){
                continue;
            }
            queue.offer(newPos);
        }
        return target;
    }

    private String GetDesc(String content, List<Term> terms) {
        int pos=-1;
        for (Term term:terms) {
            String word=term.getName();
            content=content.toLowerCase().replaceAll("\\b"+word+"\\b"," "+word+" ");
            pos=content.toLowerCase().lastIndexOf(" "+word+" ");
            if (pos>=0){
                break;
            }
        }
        if (pos==-1){
            if (content.length()>160){
                return content.substring(0,160)+"....";
            }
            return content.substring(0,content.length());
        }
        String desc="";
        int descBeg=pos<60?0:pos-60;
        if (descBeg+160>content.length()){
            desc=content.substring(descBeg,content.length());
        }else {
            desc=content.substring(descBeg,descBeg+160)+"...";
        }
        for (Term term:terms) {
            String word=term.getName();
            desc=desc.replaceAll("(?i) "+word+" "," <i>"+word+"</i> ");
        }
        return desc;
    }
    public void loadStopWords(){
        try(BufferedReader bufferedReader =new BufferedReader(new FileReader(STOP_WORD_PATH))) {
            while (true){
                String line=bufferedReader.readLine();
                if (line==null){
                    break;
                }
                stopWords.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        DocSearcher docSearcher=new DocSearcher();
        Scanner in=new Scanner(System.in);
        while (true){
            String query=in.nextLine();
            List<Result> list=docSearcher.search(query);
            for (Result result:list) {
                System.out.println("================================");
                System.out.println(result);
            }
        }

    }
}
