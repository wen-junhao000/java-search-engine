package searcher;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class Parser {
    private static final String INPUT_PATH="D:\\javaproject\\jdk-8u321-docs-all\\docs\\api\\";
    private Index index=new Index();
    private AtomicLong t1=new AtomicLong(0);
    private AtomicLong t2=new AtomicLong(0);
    public void runByThread() throws IOException, InterruptedException {
        long beg=System.currentTimeMillis();
        System.out.println("索引制作开始");
        ArrayList<File> fileList=new ArrayList<File>();
        enumFile(INPUT_PATH,fileList);
        long endEnumFile=System.currentTimeMillis();
        System.out.println("枚举时间完毕！消耗时间："+(endEnumFile-beg));
        final CountDownLatch latch=new CountDownLatch(fileList.size());
        ExecutorService executorService= Executors.newFixedThreadPool(4);
        for (final File f:fileList) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("开始解析："+f.getAbsolutePath());
                    parseHTML(f);
                    latch.countDown();
                }
            });
        }
        latch.await();
        executorService.shutdown();
        System.out.println("循环遍历完毕："+(System.currentTimeMillis()-endEnumFile));
        index.save();
        long end=System.currentTimeMillis();
        System.out.println("索引制作结束 消耗时间："+(end-beg)+"ms");
        System.out.println("t1:"+t1+"t2:"+t2);
    }
    public void run() throws IOException {
        long beg=System.currentTimeMillis();
        System.out.println("索引制作开始");

        ArrayList<File> fileList=new ArrayList<File>();
        enumFile(INPUT_PATH,fileList);
        long endEnumFile=System.currentTimeMillis();
        System.out.println("枚举时间完毕！消耗时间："+(endEnumFile-beg));
        for (File file:fileList) {
            System.out.println("开始解析："+file.getAbsolutePath());
            parseHTML(file);
        }
        System.out.println("循环遍历完毕："+(System.currentTimeMillis()-endEnumFile));
        index.save();
        long end=System.currentTimeMillis();
        System.out.println("索引制作结束 消耗时间："+(end-beg));
//        System.out.println(fileList.size());

    }

    private void parseHTML(File f) {
         String title=parseTitle(f);
         String url=parseUrl(f);
         long beg=System.nanoTime();
         String content=parseContentByRegex(f);
         long mid=System.nanoTime();
         index.addDoc(title,url,content);
         long end=System.nanoTime();
         t1.addAndGet(mid-beg);
         t2.addAndGet(end-mid);

    }

    private String parseTitle(File f) {
        return f.getName().substring(0,f.getName().lastIndexOf('.'));
    }

    private String parseUrl(File f) {
        String part1="https://docs.oracle.com/javase/8/docs/api/";
        String part2=f.getAbsolutePath().substring(INPUT_PATH.length());
        return part1+part2;
    }

    private String parseContent(File f) {
        try(BufferedReader bufferedReader=new BufferedReader(new FileReader(f),1024*1024)) {
            boolean isCopy=true;
            StringBuilder content=new StringBuilder();
            while (true){
                int ch=bufferedReader.read();
                if (ch==-1){
                    break;
                }
                char c=(char)ch;
                if (isCopy){
                    if (c=='<'){
                        isCopy=false;
                        continue;
                    }if (c=='\n'||c=='\r'){
                        c=' ';
                    }
                    content.append(c);
                }else {
                    if (c=='>'){
                        isCopy=true;
                        continue;
                    }
                }
            }
            bufferedReader.close();
            return content.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
    private String readFile(File f){
        try(BufferedReader bufferedReader=new BufferedReader(new FileReader(f))) {
            StringBuilder content=new StringBuilder();
            while (true){
                int ret=bufferedReader.read();
                if (ret==-1){
                    break;
                }
                char ch=(char)ret;
                if (ch=='\n'||ch=='\r'){
                    ch=' ';
                }
                content.append(ch);
            }
            return content.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
    public String parseContentByRegex(File f){
        String  content=readFile(f);
        content=content.replaceAll("<script.*?>(.*?)</script>"," ");
        content=content.replaceAll("<.*?>"," ");
        content=content.replaceAll("\\s+"," ");
        return content;
    }
    private void enumFile(String inputPath, ArrayList<File> fileList) {
        File rootPath=new File(inputPath);
        File[] files=rootPath.listFiles();
        for (File file:files) {
            if (file.isDirectory()){
                enumFile(file.getAbsolutePath(),fileList);
            }else if (file.getAbsolutePath().endsWith(".html")){
                fileList.add(file);
            }
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        Parser parser=new Parser();
        parser.runByThread();
    }
}